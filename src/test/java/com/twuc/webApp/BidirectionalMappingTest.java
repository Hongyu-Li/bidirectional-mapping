package com.twuc.webApp;

import com.twuc.webApp.entities.Office;
import com.twuc.webApp.entities.OfficeRepository;
import com.twuc.webApp.entities.Staff;
import com.twuc.webApp.entities.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class BidirectionalMappingTest {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void should_save_into_both_repository_successfully_from_many() {
        Office office = officeRepository.saveAndFlush(new Office("Shanghai"));
        Staff staff = staffRepository.saveAndFlush(new Staff(">_<"));
        entityManager.clear();

        final Office o = officeRepository.findById(office.getId()).orElseThrow(NoSuchElementException::new);
        final Staff s = staffRepository.findById(staff.getId()).orElseThrow(NoSuchElementException::new);
        s.setOffice(o);
        staffRepository.flush();
        entityManager.clear();
        assertEquals(1,officeRepository.findById(office.getId()).get().getStaffs().size());
    }

    @Test
    void should_remove_child_from_one_side() {
        Office office = officeRepository.saveAndFlush(new Office("Shanghai"));
        Staff staff = staffRepository.saveAndFlush(new Staff(">_<"));
        staff.setOffice(office);
        entityManager.flush();
        entityManager.clear();

        officeRepository.findById(office.getId()).get().getStaffs().clear();
        officeRepository.flush();
        entityManager.clear();
        assertEquals(0,officeRepository.findById(office.getId()).get().getStaffs().size());
    }
}
