package com.twuc.webApp.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Office {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true, mappedBy = "office")
    private List<Staff> staffs = new ArrayList<>();

    public Office() {
    }

    public Office(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Staff> getStaffs() {
        return staffs;
    }


    public void addStaff(Staff staff){
        if(staffs.contains(staff)){
            return;
        }
        staffs.add(staff);
        staff.setOffice(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Office office = (Office) o;
        return Objects.equals(id, office.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
